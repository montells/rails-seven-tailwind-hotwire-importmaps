module SiteHelper
  def rand_color
    color = "%06x" % (rand * 0xffffff)
    "##{color}"
  end
end
